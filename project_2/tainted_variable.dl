#include "./cs_control_flow.dl"
#include "./dominates.dl"

/// Dangerous variable is one that is defined by CALLDATALOAD operation (more maybe to be added later):
.decl DangerousVariable(var:Variable)
.output DangerousVariable

DangerousVariable(var) :-
    Statement_Opcode(statement, "CALLDATALOAD"),
    Statement_Defines(statement, var, _).

DangerousVariable(var) :-
    DangerousVariable(from),
    CS_Flows(from, var).

/// A statement is sensitive if it is either of the following (more maybe to be added later):
/// a) DELEGATECALL
/// b) CALLCODE
.decl SensitiveStatement(statement:Statement)
.output SensitiveStatement

SensitiveStatement(statement) :-
    Statement_Opcode(statement, "DELEGATECALL").

SensitiveStatement(statement) :-
    Statement_Opcode(statement, "CALLCODE").

/// Sensitive context is a pair of (var,block)
.decl SensitiveContext(var:Variable, block:Block)
.output SensitiveContext

SensitiveContext(var, block) :-
    SensitiveStatement(statement),
    Statement_Uses(statement, var, _),
    Statement_Block(statement, block).

/// A statement is a guard to a dangerous statement if it checks the caller of the contract
.decl GuardBlock(guard_block:Block)
.output GuardBlock

GuardBlock(guard_block) :-
    Statement_Opcode(caller_statement, "CALLER"),
    Statement_Defines(caller_statement, caller_var, _),
    Statement_Opcode(jump_statement, "JUMPI"),
    Statement_Uses(jump_statement, jump_var, _),
    CS_Flows(caller_var, jump_var),
    Statement_Block(jump_statement, guard_block).

/// A variable is tainted if one of the following holds:
/// a) a dangerous variable has flow to a sensitive variable without a preceding guard
/// b) a certified tainted variable has flow to it.
.decl TaintedVariable(var:Variable)
.output TaintedVariable

TaintedVariable(dangerous_var) :-
    DangerousVariable(dangerous_var),
    SensitiveContext(sensitive_var, sensitive_block),
    CS_Flows(dangerous_var, sensitive_var),
    GuardBlock(guard_block),
    !Dominates(guard_block, sensitive_block).
