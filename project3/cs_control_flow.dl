#include "../clientlib/decompiler_imports.dl"
#include "./dominates.dl"

/// Dangerous variable is one that is defined by CALLDATALOAD operation (more maybe to be added later):
.decl DangerousVariable(var:Variable)

DangerousVariable(var) :-
    Statement_Opcode(statement, "CALLDATALOAD"),
    Statement_Defines(statement, var, _).

DangerousVariable(var) :-
    DangerousVariable(from),
    CS_Flows(from, var).

/// A statement is sensitive if it is either of the following (more maybe to be added later):
/// a) DELEGATECALL
/// b) CALLCODE
.decl SensitiveStatement(statement:Statement)

SensitiveStatement(statement) :-
    Statement_Opcode(statement, "DELEGATECALL").

SensitiveStatement(statement) :-
    Statement_Opcode(statement, "CALLCODE").

/// Sensitive context is a pair of (var,block)
.decl SensitiveContext(var:Variable, block:Block)

SensitiveContext(var, block) :-
    SensitiveStatement(statement),
    Statement_Uses(statement, var, _),
    Statement_Block(statement, block).

/// A statement is a guard to a dangerous statement if it checks the caller of the contract
.decl GuardBlock(guard_block:Block)

GuardBlock(guard_block) :-
    Statement_Opcode(caller_statement, "CALLER"),
    Statement_Defines(caller_statement, caller_var, _),
    Statement_Opcode(jump_statement, "JUMPI"),
    Statement_Uses(jump_statement, jump_var, _),
    CS_Flows(caller_var, jump_var),
    Statement_Block(jump_statement, guard_block).

/// A variable is tainted if one of the following holds:
/// a) a dangerous variable has flow to a sensitive variable without a preceding guard
/// b) a certified tainted variable has flow to it.
.decl TaintedVariable(var:Variable)

TaintedVariable(dangerous_var) :-
    DangerousVariable(dangerous_var),
    SensitiveContext(sensitive_var, sensitive_block),
    CS_Flows(dangerous_var, sensitive_var),
    GuardBlock(guard_block),
    !Dominates(guard_block, sensitive_block).

.decl UDFlows(from:Variable, to:Variable)

/// Uses-Defines Flows
UDFlows(from, to) :-
    Statement_Uses(statement, from, _),
    Statement_Defines(statement, to, _),
    FlowOp(op),
    Statement_Opcode(statement, op).

/// PHI Flows
.decl PHIFlows(from:Variable, to:Variable)

PHIFlows(from, to) :-
    PHI("PHI", from, to).

/// ArgsToFormalArgsFlows
.decl ArgsToFormalArgsFlows(context:Block, from:Variable, to:Variable)

ArgsToFormalArgsFlows(caller_block, from, to) :-
    ActualArgs(caller_block, from, i),
    CallGraphEdge(caller_block, func),
    FormalArgs(func, to, i).

/// FormalReturnToActualReturnFlows
.decl FormalReturnToActualReturnFlows(context:Block, from:Variable, to:Variable)

FormalReturnToActualReturnFlows(caller_block, from, to) :-
    FormalReturnArgs(func, from, i),
    CallGraphEdge(caller_block, func),
    ActualReturnArgs(caller_block, to, i).

/// ArgsFlows
.decl ArgsFlows(from:Variable, to:Variable)

ArgsFlows(arg, ararg) :-
    ArgsToFormalArgsFlows(caller_block, arg, farg),
    RegularFlows(farg, frarg),
    FormalReturnToActualReturnFlows(caller_block, frarg, ararg).

///  MemoryFlows
/// GeneralizedVariableValue
.decl GeneralizedVariableValue(var: Variable, value: Value)

GeneralizedVariableValue(var, value) :-
    IsVariable(var),
    RegularFlows(var, tmp),
    Variable_Value(tmp, value).

GeneralizedVariableValue(var, cat("input", statement)) :-
    Statement_Opcode(statement, "CALLDATALOAD"),
    Statement_Defines(statement, var, _).

GeneralizedVariableValue(hashed_word, cat(cat("SHA3(", sym_val), ")")) :-
    Statement_Opcode(calldataload, "CALLDATALOAD"),
    Statement_Defines(calldataload, var, _),            // var was initialized by calldataload
    GeneralizedVariableValue(var, sym_val),             // var holds the symbolic value sym_val
    RegularFlows(var, var_2),                           // var flows to var_2
    VariableStoredInLocation(var_2, pos),               // var_2 was in position pos in memory
    Statement_Opcode(sha3, "SHA3"),                     // let a SHA3 operation
    Statement_Uses(sha3, startAddr, 0),                 // startAddr holds the (starting) address to be hashed
    GeneralizedVariableValue(startAddr, pos),           // assert starting address is same as var_2 address
    Statement_Defines(sha3, hashed_word, _).            // the defined variable has the value SHA3(sym_val)

// Store Operations
.decl StoreOp(stmt:Statement, index:Variable, var:Variable)

StoreOp(stmt, index, var) :- SSTORE(stmt, index, var); MSTORE(stmt, index, var).

// Load Operations
.decl LoadOp(stmt:Statement, index:Variable, var:Variable)

LoadOp(stmt, index, var) :- SLOAD(stmt, index, var); MLOAD(stmt, index, var).

// VariableStoredInLocation
.decl VariableStoredInLocation(var:Variable, pos:Value)

VariableStoredInLocation(from, pos) :-
    StoreOp(_, index_var, from),
    GeneralizedVariableValue(index_var, pos).

// VariableLoadedFromLocation
.decl VariableLoadedFromLocation(var:Variable, pos:Value)

VariableLoadedFromLocation(to, pos) :-
    LoadOp(_, index_var, to),
    GeneralizedVariableValue(index_var, pos).

// Memory Flow
.decl MemoryFlows(from:Variable, to:Variable)

MemoryFlows(from, to) :-
    VariableStoredInLocation(from, index),
    VariableLoadedFromLocation(to, index).

/// RegularFlowsEdge
.decl RegularFlowsEdge(from:Variable, to:Variable)

RegularFlowsEdge(from, to) :-
    MemoryFlows(from, to).

RegularFlowsEdge(var, var) :-
    IsVariable(var).

RegularFlowsEdge(from, to) :-
    UDFlows(from, to).

RegularFlowsEdge(from, to) :-
    PHIFlows(from, to).

RegularFlowsEdge(from, to) :-
    ArgsFlows(from, to).

RegularFlowsEdge(from, to) :-
    ArgsToFormalArgsFlows(_, from, to).

/// RegularFlows
.decl RegularFlows(from:Variable, to:Variable)

RegularFlows(from, to) :-
    RegularFlowsEdge(from, to).

RegularFlows(from, to) :-
    RegularFlows(from, t1),
    RegularFlowsEdge(t1, to).

//CS_Flows
.decl CS_Flows(from:Variable, to:Variable)
.output CS_Flows

CS_Flows(from, to) :-
    RegularFlows(from,to).

/// VariableStoredInUnknownLocation
.decl VariableStoredInUnknownLocation(var:Variable)

VariableStoredInUnknownLocation(var) :-
    StoreOp(_, index, var),
    !GeneralizedVariableValue(index, _).

// UnknownStoreFlows
.decl UnknownStoreFlows(from:Variable, to:Variable)

UnknownStoreFlows(from, to) :-
     VariableStoredInUnknownLocation(from),
     VariableLoadedFromLocation(to, _).

// GeneralFlowsEdge
.decl GeneralFlowsEdge(from:Variable, to:Variable)

GeneralFlowsEdge(from, to) :-
    RegularFlowsEdge(from, to).

GeneralFlowsEdge(from, to) :-
    UnknownStoreFlows(from, to).

/// GeneralFlows
.decl GeneralFlows(from:Variable, to:Variable)
.output GeneralFlows

GeneralFlows(from, to) :-
    GeneralFlowsEdge(from, to).

GeneralFlows(from, to) :-
    GeneralFlows(from, tmp),
    GeneralFlowsEdge(tmp, to).

/// VariableStoredInUnknownTaintedLocation
.decl VariableStoredInUnknownTaintedLocation(var:Variable)

VariableStoredInUnknownTaintedLocation(var) :-
    VariableStoredInUnknownLocation(var),
    TaintedVariable(var).

// SemiGeneralFlows
.decl SemiGeneralFlowsEdge(from:Variable, to:Variable)

SemiGeneralFlowsEdge(from, to) :-
    RegularFlowsEdge(from, to).

SemiGeneralFlowsEdge(from, to) :-
    VariableStoredInUnknownTaintedLocation(from),
    VariableLoadedFromLocation(to, _).

// SemigeneralFlows
.decl SemiGeneralFlows(from:Variable, to:Variable)
.output SemiGeneralFlows

SemiGeneralFlows(from, to) :-
    SemiGeneralFlowsEdge(from, to).

SemiGeneralFlows(from, to) :-
    SemiGeneralFlowsEdge(from, tmp),
    SemiGeneralFlows(tmp, to).
